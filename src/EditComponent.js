import React, { Component } from 'react';
import { connect } from 'react-redux';


class EditComponent extends Component {

constructor(props) {
    super(props)
    this.state = { category: 'phones'}
    this.handleChange = this.handleChange.bind(this);
}

handleChange(event) {
	this.setState({category: event.target.value});
}
	
handleEdit = (e) => {
	e.preventDefault();
	const name = this.getName.value;
	const price = this.getPrice.value;
	const category = this.state.category;
	const description = this.getDescription.value;
	const data = {
		name,
		price,
		category,
		description
	}
	this.props.dispatch({ type: 'UPDATE', id: this.props.post._id, data: data })
}
render() {
return (
<div key={this.props.post.id} className="post">
	<form className="form" onSubmit={this.handleEdit}>
		<input required type="text" ref={(input) => this.getName = input}
   		defaultValue={this.props.post.name} placeholder="Enter Name"/>
   
   		<input required type="text" ref={(input) => this.getPrice = input}
   		defaultValue={this.props.post.price} placeholder="Enter Price"/>
   
   		<label className= "category">
          Pick Category:
          <select value={this.state.category} onChange={this.handleChange}>
            <option value="phones">phones</option>
            <option value="computers">computers</option>
            <option value="tvs">tvs</option>
          </select>
        </label>
   
   		<textarea required rows="5" ref={(input) => this.getDescription = input}
   		cols="28" defaultValue={this.props.post.description} placeholder="Enter Description" /><br /><br />
		<button>Update</button>
	</form>
</div>
);
}
}
export default connect()(EditComponent);