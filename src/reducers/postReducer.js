const postReducer = (state = [], action) => {
	switch (action.type) {
		case 'ADD_STATE':
			return state.concat([action.data])
		case 'ADD_POST':
			return fetch('http://localhost:3001/api/product', {
		        method: 'POST',
		        body: JSON.stringify(action.data),
		        headers: {
		            'Content-Type': 'application/json'
		        }
		    }).then(res => res.json())
		    .then(res => {
		    	window.location.reload();
			})
		case 'DELETE_POST':
			return fetch('http://localhost:3001/api/product/' + action.id, {
		        method: 'DELETE'
		    }).then(res => res.json())
		    .then(res => {
		    	window.location.reload();
			})
		    .catch(err => console.log(err));
		case 'EDIT_POST':
			return state.map((post) => post._id === action.id ? { ...post, editing: !post.editing }  : post)
		case 'UPDATE':
			return state.map((post) => {
				console.log(action)
				if (post._id === action.id) {
					return fetch('http://localhost:3001/api/product/' + action.id, {
				        method: 'PUT',
				        body: JSON.stringify(action.data),
				        headers: {
				            'Content-Type': 'application/json'
				        }
				    }).then(res => res.json())
				    .then(res => {
				    	window.location.reload();
					})
				    .catch(err => console.log(err)); 
				} else return post;
			})
		default:
			return state;
	}
}
export default postReducer;