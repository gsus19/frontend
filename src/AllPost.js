import React, { Component } from 'react';
import { connect } from 'react-redux';
import Post from './Post';
import EditComponent from './EditComponent';
import { RingLoader } from 'react-spinners';



class AllPost extends Component {

	constructor(props) {
	    super(props)
	    this.state = { 
	        products: [],
	        loading: false
	    }
	}


    componentWillMount() {
       getAll(this)
    }

	render() {
		let screen;
			if (this.state.loading){
				screen = <div className='sweet-loading'>
					<RingLoader
						color={'#123abc'} 
					  	loading={this.state.loading} 
					/>
				</div>	
			}

			else {
				screen = <div>
				  	<h1 className="post_heading">Products</h1>
				  	{this.props.posts.map((post) => (
					  	<div key={post._id}>
					    	{post.editing ? <EditComponent post={post} key={post._id} /> : <Post post={post}
					    	key={post._id} />}
					  	</div>
					))}
				</div>	
			}


		return (
			
			<div>
				{screen}
			</div>
			
		);
	}
}

function getAll(self){
	self.state.loading = true
	fetch('http://localhost:3001/api/product')
	.then((response) => {
	    return response.json()
	})
	.then((data) => {
		data.products.map((product)=>{
	    	self.props.dispatch({
			 type: 'ADD_STATE',
			 data: product
			 })
		})
	})
	.catch(err => console.log(err));
	self.state.loading = false
}


const mapStateToProps = (state) => {
	return {
		posts: state
	}
}
export default connect(mapStateToProps)(AllPost);