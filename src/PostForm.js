import React, { Component } from 'react';
import { connect } from 'react-redux';
class PostForm extends Component {

constructor(props) {
    super(props)
    this.state = { category: 'phones'}
    this.handleChange = this.handleChange.bind(this);
}

handleChange(event) {
	this.setState({category: event.target.value});
}


handleSubmit = (e) => {
	e.preventDefault();
	const name = this.getName.value;
	const price = this.getPrice.value;
	const category = this.state.category;
	const description = this.getDescription.value;
	const data = {
	_id: Date.now().toString(),
	name,
	price,
	category,
	description,
	editing: false
}

this.props.dispatch({
	 type: 'ADD_POST',
	 data: data
	 })
	 this.getName.value = '';
	 this.getPrice.value = '';
	 this.getDescription.value = '';
}
render() {
return (
<div className="post-container">
  	<h1 className="post_heading">Create Product</h1>
  	<form className="form" onSubmit={this.handleSubmit}>
   		<input required type="text" ref={(input) => this.getName = input}
   		placeholder="Enter Name"/>
   
   		<input required type="number" ref={(input) => this.getPrice = input}
   		placeholder="Enter Price"/>
   
		<label className= "category">
          Pick Category:
          <select value={this.state.category} onChange={this.handleChange}>
            <option value="phones">phones</option>
            <option value="computers">computers</option>
            <option value="tvs">tvs</option>
          </select>
        </label>
   
   		<textarea required rows="5" ref={(input) => this.getDescription = input}
   		cols="28" placeholder="Enter Description" /><br /><br />
   
   		<button>Post</button>
  	</form>
</div>
);
}
}
export default connect()(PostForm);